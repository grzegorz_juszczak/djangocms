from django.contrib import admin
from catalog.models import Product, Category, Comment

class ProductAdmin(admin.ModelAdmin): 
    # sets values for how the admin site lists products
    list_display = ('name', 'price',)
    list_display_links = ('name',)
    list_per_page = 50
    ordering = ['name']
    search_fields = ['name', 'description']

    # sets up slug to be generated from product name
    prepopulated_fields = {'slug' : ('name',)}

admin.site.register(Product, ProductAdmin)

class CategoryAdmin(admin.ModelAdmin):
    # sets up values for how admin site lists categories
    list_display = ('name',)
    list_display_links = ('name',)
    list_per_page = 20
    ordering = ['name']
    search_fields = ['name', 'description']

    # sets up slug to be generated from category name
    prepopulated_fields = {'slug' : ('name',)}

admin.site.register(Category, CategoryAdmin)
admin.site.register(Comment)
