from django.conf.urls import include, url
from catalog import views
 
urlpatterns = [
    url(r'^category/(?P<category_slug>[-\w]+)/$', views.show_category, name='catalog_category'),
    url(r'^product/(?P<product_slug>[-\w]+)/add-comment/$', views.add_comment, name='add_comment'),
    url(r'^product/(?P<product_slug>[-\w]+)/$', views.show_product, name='catalog_product'),
]