from django import template
from cart import cart_logic

register = template.Library()

@register.inclusion_tag("cart/cart_box.html")
def cart_box(request):
    cart_item_count = cart_logic.cart_distinct_item_count(request)
    return {'cart_item_count': cart_item_count}
