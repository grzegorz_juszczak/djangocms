from django.conf.urls import url
from django.views.generic import RedirectView

urlpatterns = [
    # redirect to category: All
    url(r'^$', RedirectView.as_view(pattern_name='catalog_category'), name='main_home', kwargs={'category_slug':'all'}),
]