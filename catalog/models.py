from __future__ import unicode_literals
from django.core.urlresolvers import reverse
from django.db import models

class Category(models.Model):
    name = models.CharField(max_length=50)
    slug = models.SlugField(max_length=50, unique=True, help_text='Unique value for product page URL, created from name.')
    description = models.TextField()

    def __unicode__(self):
        return self.name

    def get_absolute_url(self):
        return reverse('catalog_category', args=(self.slug,))

    class Meta:
        db_table = 'categories'
        ordering = ['name']
        verbose_name_plural = 'Categories'

class Product(models.Model):
    name = models.CharField(max_length=255, unique=True)
    slug = models.SlugField(max_length=255, unique=True, help_text='Unique value for product page URL, created from name.')
    brand = models.CharField(max_length=50)
    price = models.DecimalField(max_digits=9, decimal_places=2)
    image = models.FileField(upload_to='./product_img/')
    quantity = models.IntegerField()
    description = models.TextField()
    categories = models.ManyToManyField(Category)

    def __unicode__(self):
        return self.name

    def get_absolute_url(self):
        return reverse('catalog_product', args=(self.slug,))

    class Meta:
        db_table = 'products'
        ordering = ['name']


class Comment(models.Model):
    text = models.TextField()
    author = models.CharField(max_length=255)
    publish_date = models.DateTimeField(auto_now=True)
    product = models.ForeignKey(Product)

    def __unicode__(self):
        return self.text

    class Meta:
        db_table = 'comments'

