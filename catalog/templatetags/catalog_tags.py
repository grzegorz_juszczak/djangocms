from django import template
from catalog.models import Category

register = template.Library()

@register.inclusion_tag("catalog/category_list.html")
def category_list(request_path):
    active_categories = Category.objects.exclude(slug='all')
    return {'active_categories': active_categories, 'request_path': request_path}
