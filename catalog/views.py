from django.shortcuts import get_object_or_404, render
from django.core import urlresolvers
from django.http import HttpResponseRedirect, HttpResponseNotFound
from cart.forms import ProductAddToCartForm
from cart import cart_logic
from catalog.models import Category, Product, Comment

# Create your views here.
def show_category(request, category_slug):
    category = get_object_or_404(Category, slug=category_slug)
    products = category.product_set.all()
    request.session.set_test_cookie()
    return render(request, 'catalog/category.html', locals())

def show_product(request, product_slug):
    p = get_object_or_404(Product, slug=product_slug)
    categories = p.categories.all()
    comments = Comment.objects.filter(product=p).order_by('-publish_date')
    # need to evaluate the HTTP method
    if request.method == 'POST':
        # add to car create the bound form
        postdata = request.POST.copy()
        form = ProductAddToCartForm(request, postdata)
        #check if posted data is valid
        if form.is_valid():
            #add to cart and redirect to cart
            cart_logic.add_to_cart(request)
            # if test cookie worked, get rid of it
            if request.session.test_cookie_worked():
                request.session.delete_test_cookie()
                url = urlresolvers.reverse('show_cart')
                return HttpResponseRedirect(url)
    else:
        # it's a GET create the unbound form. Note request as a kwarg
        form = ProductAddToCartForm(request=request, label_suffix=':')
    # assign the hidden input the product slug
    form.fields['product_slug'].widget.attrs['value'] = product_slug
    # set the test cookie on our first GET request
    request.session.set_test_cookie()
    return render(request, 'catalog/product.html', locals())

def add_comment(request, product_slug):
    if request.method == 'POST':
        p = get_object_or_404(Product, slug=product_slug)
        text = request.POST.get('text')
        comment = Comment(text=text, author=request.user.username, product=p)
        comment.save()
        url = urlresolvers.reverse('catalog_product', kwargs={'product_slug':product_slug})
        return HttpResponseRedirect(url)
    else:
        return HttpResponseNotFound()
    