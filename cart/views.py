from django.shortcuts import render

# Create your views here.
from django.shortcuts import render_to_response
from . import cart_logic

def show_cart(request):      
    if request.method == 'POST':
        postdata = request.POST.copy()
        if postdata['submit'] == 'Remove':
            cart_logic.remove_from_cart(request)
        if postdata['submit'] == 'Update':
            cart_logic.update_cart(request)
    cart_items = cart_logic.get_cart_items(request)
    cart_subtotal = cart_logic.cart_subtotal(request)
    return render(request, 'cart/cart.html', locals())